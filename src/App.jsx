import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Header from './components/Header'
import UsersPage from './pages/UsersPage'
import SpeakersPage from './pages/SpeakersPage'
import SpeechesPage from './pages/SpeechesPage';

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path="/" component={SpeechesPage} />
        <Route exact path="/users" component={UsersPage} />
        <Route exact path="/speakers" component={SpeakersPage} />
      </Switch>
    </div>
  );
}

export default App;
