export const speechesData = [
    {
        id: 1,
        time: "9:00 - 10:00",
        speakerId: 1,
        title: "Organization of successful automation",
        description: `In many resources about automation, we can find that first step in automation is choosing the right tools. But is it the best first steps? In my experience as QA Manager, I saw many failed automation projects with strong engineers who were successfully solving really difficult technical changes but they failed in an organization of automation process. They miss such things as test cases prioritization, MVP, the balance between automation value and framework complexity, helping POs and BAs to execute UAT in different environments, advertising automation framework to stakeholders.

        So in this talk, I would like to focus on organization automation project, how to make technically poor automation framework successful in the eyes of stakeholders and to have maxim value for product quality.`,
        score: 0
    },
    {
        id: 2,
        time: "11:00 - 12:00",
        speakerId: 1,
        title: "Title2",
        description: "description2",
        score: 0
    },
    {
        id: 3,
        time: "11:00 - 12:00",
        speakerId: 1,
        title: "Title2",
        description: "description2",
        score: 0
    },
    {
        id: 4,
        time: "11:00 - 12:00",
        speakerId: 1,
        title: "Title2",
        description: "description2",
        score: 0
    }
]