import React from 'react'
import { SpeechesList as SpeechesListComponent } from './SpeechesList'
import SpeechesList from './SpeechesList'
import { renderComponent, shallowRenderComponent } from '../_helpers/test.helper'
import { render } from '@testing-library/react'

const speeches = {
    loading: false,
    error: '',
    items: [
        {
            id: 1,
            time: "9:00 - 10:00",
            speakerId: 1,
            title: "Organization of successful automation",
            description: `In many resources about automation, we can find that first step in automation is choosing the right tools. But is it the best first steps? In my experience as QA Manager, I saw many failed automation projects with strong engineers who were successfully solving really difficult technical changes but they failed in an organization of automation process. They miss such things as test cases prioritization, MVP, the balance between automation value and framework complexity, helping POs and BAs to execute UAT in different environments, advertising automation framework to stakeholders.
    
            So in this talk, I would like to focus on organization automation project, how to make technically poor automation framework successful in the eyes of stakeholders and to have maxim value for product quality.`,
            score: 0,
            speaker: {
                id: 24,
                image: "image1",
                name: "Speaker name1",
                title: "Speaker title1",
            },
        },
        {
            id: 2,
            time: "11:00 - 12:00",
            speakerId: 1,
            title: "Title2",
            description: "description2",
            score: 0,
            speaker: {
                id: 43,
                image: "image2",
                name: "Speaker name2",
                title: "Speaker title2",
            },
        },
    ]
}

describe("SpeechesList", () => {

    it("snapshot default", () => {
        const result = shallowRenderComponent(<SpeechesListComponent speeches={speeches} />);

        expect(result).toMatchSnapshot();
    })

    it("snapshot loading", () => {
        const { asFragment } = renderComponent({ auth: {}, speeches: { loading: true } }, <SpeechesList />);

        expect(asFragment()).toMatchSnapshot();
    })

    it("snapshot error", () => {
        const { asFragment } = renderComponent({ auth: {}, speeches: { error: "errorMessage" } }, <SpeechesList />);

        expect(asFragment()).toMatchSnapshot();
    })

    it("get speeches when mount and updated", () => {
        const getSpeeches = jest.fn()
        const { rerender } = render(<SpeechesListComponent speeches={speeches} getSpeeches={getSpeeches} loggedIn={false} />);

        expect(getSpeeches).toBeCalledTimes(1); //mount

        rerender(<SpeechesListComponent speeches={speeches} getSpeeches={getSpeeches} loggedIn={false} />)

        expect(getSpeeches).toBeCalledTimes(1); //did update with false condition

        rerender(<SpeechesListComponent speeches={speeches} getSpeeches={getSpeeches} loggedIn={true} />)

        expect(getSpeeches).toBeCalledTimes(2); //mount + update
    })
})  