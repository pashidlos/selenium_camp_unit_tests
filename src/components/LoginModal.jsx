import React, { useState } from 'react'
import { connect } from "react-redux";
import { authActions } from '../redux/actions';
import {
    Button,
    Grid,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
} from '@material-ui/core'

const actionCreators = {
    login: authActions.login,
}

function LoginModal({ isOpen, closeModal, login }) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        login({ email, password });
        closeModal()
    }

    return (
        <Dialog
            open={isOpen}
            onClose={closeModal}
            data-testid="loginModal"
        >
            <DialogTitle id="alert-dialog-title">Login</DialogTitle>
            <form onSubmit={handleSubmit}>
                <DialogContent>
                    <Grid container direction='column' spacing={2}>
                        <Grid item>
                            <TextField
                                id="email"
                                name="email"
                                value={email}
                                label={'Email address'}
                                type="text"
                                variant="outlined"
                                required
                                fullWidth
                                inputProps={{
                                    onChange: event => setEmail(event.target.value),
                                    'data-testid': 'email',
                                }}
                            />
                        </Grid>

                        <Grid item>
                            <TextField
                                id="password"
                                name="password"
                                value={password}
                                label={'Password'}
                                type="password"
                                variant="outlined"
                                required
                                fullWidth
                                inputProps={{
                                    onChange: event => setPassword(event.target.value),
                                    'data-testid': 'password',
                                }}
                            />
                        </Grid>
                    </Grid>

                </DialogContent>
                <DialogActions>
                    <Button onClick={closeModal} color="primary" data-testid="closeBtn">
                        Close
                        </Button>
                    <Button type="submit" color="primary" data-testid="loginBtn">
                        Login
                        </Button>
                </DialogActions>
            </form>
        </Dialog>
    )
}

export default connect(
    null,
    actionCreators
)(LoginModal);
