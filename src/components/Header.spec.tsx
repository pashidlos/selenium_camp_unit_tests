import React from 'react'
import { fireEvent, wait, waitForElementToBeRemoved } from '@testing-library/react'
import { authConstants } from '../redux/constants'
import Header from './Header'
import { renderComponent } from '../_helpers/test.helper'

describe('Header', () => {
  it('snapshoot guest', () => {
    const state = {
      auth: {
        loggedIn: false,
      },
      users: {},
      speakers: {},
      speeches: {},
      alert: {}
    }

    const { asFragment } = renderComponent(state, <Header />)

    expect(asFragment()).toMatchSnapshot();
  });

  it('snapshoot logged in', () => {
    const state = {
      auth: {
        loggedIn: true,
      },
      users: {},
      speakers: {},
      speeches: {},
      alert: {}
    }

    const { asFragment } = renderComponent(state, <Header />)

    expect(asFragment()).toMatchSnapshot();
  });

  it('can dispatch logout action', () => {
    const state = {
      auth: {
        loggedIn: true,
      },
      users: {},
      speakers: {},
      speeches: {},
      alert: {}
    }

    const { getByTestId, store } = renderComponent(state, <Header />)

    fireEvent.click(getByTestId('logoutBtn'))

    let action = store.getActions()
    expect(action[0].type).toBe(authConstants.LOG_OUT);
  });

  it('can toggle login modal', async () => {
    const state = {
      auth: {
        loggedIn: false
      },
      users: {},
      speakers: {},
      speeches: {},
      alert: {}
    }

    const { getByTestId } = renderComponent(state, <Header />)

    fireEvent.click(getByTestId('loginBtn'))
    expect(getByTestId('loginModal')).toBeTruthy()

    fireEvent.click(getByTestId('closeBtn'))
    await waitForElementToBeRemoved(() => getByTestId('loginModal'));
  });
})
