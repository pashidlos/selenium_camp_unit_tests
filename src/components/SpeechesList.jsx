import React from "react";
import { connect } from "react-redux";
import { speechesActions } from '../redux/actions';
import SpeechesListItem from './SpeechesListItem'

const mapState = state => {
    return {
        speeches: state.speeches,
        loggedIn: state.auth.loggedIn
    };
};

const actionCreators = {
    getSpeeches: speechesActions.getAll,
}

export class SpeechesList extends React.Component {
    componentDidMount() {
        this.props.getSpeeches();
    }

    componentDidUpdate(prevState) {
        if (prevState.loggedIn !== this.props.loggedIn) {
            this.props.getSpeeches();
        }
    }

    render() {
        const { speeches } = this.props
        return (
            <div>
                {speeches.loading && <em>Loading speeches...</em>}
                {speeches.error && <span className="text-danger">ERROR: {speeches.error}</span>}
                {speeches.items && speeches.items.map(speech => <SpeechesListItem key={speech.id} speech={speech}/>)}
            </div>
        )
    }
}

export default connect(
    mapState,
    actionCreators
)(SpeechesList);