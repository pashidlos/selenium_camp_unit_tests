import React from 'react'
import { fireEvent } from '@testing-library/react'
import LoginModal from './LoginModal'
import { renderComponent } from '../_helpers/test.helper'
import { authService } from '../services'

describe('LoginModal', () => {
    it('snapshoot', () => {
        const { getByTestId } = renderComponent({}, <LoginModal isOpen={true} />)

        expect(getByTestId('loginModal')).toMatchSnapshot();
    });

    it('can login', () => {
        authService.login = jest.fn()
            .mockImplementation(data =>
                Promise.resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(data)) }))
        const closeModalFn = jest.fn()
        const { getByTestId } = renderComponent({}, <LoginModal isOpen={true} closeModal={closeModalFn} />)

        fireEvent.change(getByTestId('email'), { target: { value: 'email@gmail.com' } })
        fireEvent.change(getByTestId('password'), { target: { value: '123456' } })
        fireEvent.click(getByTestId('loginBtn'))

        expect(authService.login).toBeCalledWith('email@gmail.com', '123456');
        expect(closeModalFn).toHaveBeenCalledTimes(1);
    });
})
