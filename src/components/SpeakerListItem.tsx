import React from "react";
import { Typography, Avatar } from "@material-ui/core";
import { Speaker } from "../types";

export const SpeakerListItem = ({ speaker, noImage }: { speaker: Speaker, noImage?: boolean }) => {
    return <div>
        {!noImage && <Avatar
            src={speaker.image}
        />}
        <Typography variant='h6'>
            {speaker.name}
        </Typography>
        <Typography>
            {speaker.title}
        </Typography>
    </div>
}