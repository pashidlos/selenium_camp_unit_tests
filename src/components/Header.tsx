import React, { FunctionComponent } from "react";
import {
    AppBar,
    Toolbar,
    Typography,
    Button,
    Grid,
} from '@material-ui/core'
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { authActions } from '../redux/actions';
import LoginModal from './LoginModal'
import { RootState } from "../redux/reducers";
import { IAuthReducer } from "../redux/reducers/auth.reducer";

interface IProps {
    auth: IAuthReducer
    logout: () => void
}

const mapState = (state: RootState) => {
    return {
        auth: state.auth,
    };
};

const actionCreators = {
    logout: authActions.logout
}

const Header: FunctionComponent<IProps> = ({ auth, logout }) => {
    const [isLoginModalShown, setIsLoginModalShown] = React.useState(false);

    const handleLogout = (event: React.MouseEvent) => {
        event.preventDefault();
        logout();
    }

    return (
        <AppBar position="static" color="default">
            <Toolbar>
                <Grid container justify="space-between">
                    <Grid item>
                        <Grid container spacing={2}>
                            <Grid item>
                                <Link to='/' >
                                    <img
                                        src="/logo512.png"
                                        width="40"
                                        height="40"
                                        alt="logo"
                                    />
                                </Link>
                            </Grid>
                            <Grid item>
                                <Typography variant="h6" component={Link} to='/'>Home</Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="h6" component={Link} to='/speakers'>Speakers</Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="h6" component={Link} to='/users'>Users</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item>
                        {auth.loggedIn ?
                            <Button
                                data-testid="logoutBtn"
                                onClick={handleLogout}
                            >
                                Logout
                            </Button>
                            :
                            <Button
                                data-testid="loginBtn"
                                onClick={() => setIsLoginModalShown(true)}
                            >
                                Login
                            </Button>
                        }
                    </Grid>
                </Grid>

            </Toolbar>
            <LoginModal
                isOpen={isLoginModalShown}
                closeModal={() => setIsLoginModalShown(false)}
            />
        </AppBar>
    );
}

export default connect(
    mapState,
    actionCreators
)(Header);