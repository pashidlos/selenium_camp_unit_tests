import React from "react";
import { connect } from "react-redux";
import { usersActions } from '../redux/actions';

const mapState = state => {
    return {
        users: state.users,
    };
};

const actionCreators = {
    getUsers: usersActions.getAll,
}

export class UserList extends React.Component {
    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        const { users } = this.props
        return (
            <div>
                {users.loading && <em>Loading users...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items && users.items.map(user => (<div key={user.id}>{user.email}</div>))}
            </div>
        )
    }
}

export default connect(
    mapState,
    actionCreators
)(UserList);