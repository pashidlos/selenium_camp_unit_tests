import React from "react";
import { connect } from "react-redux";
import { speakersActions } from '../redux/actions';
import { SpeakerListItem } from './SpeakerListItem'
import { Container, Row, Col } from "react-bootstrap";

const mapState = state => {
    return {
        speakers: state.speakers,
        loggedIn: state.auth.loggedIn
    };
};

const actionCreators = {
    getSpeakers: speakersActions.getAll,
}

export class SpeakersList extends React.Component {
    componentDidMount() {
        this.props.getSpeakers();
    }

    componentDidUpdate(prevState) {
        if (prevState.loggedIn !== this.props.loggedIn) {
            this.props.getSpeakers();
        }
    }

    render() {
        const { speakers } = this.props
        return (
            <div>
                {speakers.loading && <em>Loading speakers...</em>}
                {speakers.error && <span className="text-danger">ERROR: {speakers.error}</span>}
                {speakers.items && <Container>
                    <Row>
                        {speakers.items.map(speaker => (
                            <Col md={4} key={speaker.id}>
                                <SpeakerListItem speaker={speaker} />
                            </Col>
                        ))}
                    </Row>
                </Container>}
            </div>
        )
    }
}

export default connect(
    mapState,
    actionCreators
)(SpeakersList);