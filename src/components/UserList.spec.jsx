import React from 'react'
import { UserList as UserListComponent } from './UserList'
import UserList from './UserList'
import { renderComponent, shallowRenderComponent } from '../_helpers/test.helper'
import { render } from '@testing-library/react'

const users = {
    loading: false,
    error: '',
    items: [
        {
            id: 1,
            firstName: "Name",
            lastName: "last name",
            email: "email@gmail.com",
        },
        {
            id: 2,
            firstName: "Name",
            lastName: "last name",
            email: "email2@gmail.com",
        },
    ]
}

describe("UserList", () => {

    it("snapshot default", () => {
        const { asFragment } = renderComponent({ users }, <UserList />);

        expect(asFragment()).toMatchSnapshot();
    })

    it("snapshot loading", () => {
        const { asFragment } = renderComponent({ auth: {}, users: { loading: true } }, <UserList />);

        expect(asFragment()).toMatchSnapshot();
    })

    it("snapshot error", () => {
        const { asFragment } = renderComponent({ auth: {}, users: { error: "errorMessage" } }, <UserList />);

        expect(asFragment()).toMatchSnapshot();
    })

    it("get users when mount", () => {
        const getUsers = jest.fn()
        const { rerender } = render(<UserListComponent users={users} getUsers={getUsers} />);

        expect(getUsers).toBeCalledTimes(1); //mount
    })
})  