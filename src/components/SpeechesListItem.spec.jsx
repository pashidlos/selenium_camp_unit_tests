import React from 'react'
import { render } from '@testing-library/react'
import { renderComponent, shallowRenderComponent } from '../_helpers/test.helper'
import SpeechesListItem from './SpeechesListItem'

const speech = {
    id: 24,
    time: "speech time",
    title: "speech title",
    speaker: {
        id: 24,
        image: "image",
        name: "Speaker name",
        title: "Speaker title",
    },
}

describe("SpeechesListItem", () => {

    it("snapshot default", () => {
        const result = shallowRenderComponent(
            <SpeechesListItem speech={speech} />
        )

        expect(result).toMatchSnapshot();
    })
})