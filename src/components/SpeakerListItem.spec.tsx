import React from 'react'
import { render } from '@testing-library/react'
import { SpeakerListItem } from './SpeakerListItem'

const speaker = {
    id: 24,
    image: "image",
    name: "Speaker name",
    title: "Speaker title",
    description: "Speaker description"
}

describe("SpeakersListItem", () => {

    it("snapshot default", () => {
        const { asFragment } = render(
            <SpeakerListItem speaker={speaker} />
        )

        expect(asFragment()).toMatchSnapshot();
    })

    it("snapshot no image", () => {
        const { asFragment } = render(
            <SpeakerListItem speaker={speaker} noImage />
        )

        expect(asFragment()).toMatchSnapshot();
    })
})