import React from 'react'
import { SpeakersList as SpeakersListComponent } from './SpeakerList'
import SpeakersList from './SpeakerList'
import { renderComponent, shallowRenderComponent } from '../_helpers/test.helper'
import { render } from '@testing-library/react'

const speakers = {
    loading: false,
    error: '',
    items: [
        {
            id: 24,
            image: "image",
            name: "Speaker name",
            title: "Speaker title",
        },
        {
            id: 26,
            image: "image1",
            name: "Speaker name1",
            title: "Speaker title1",
        }
    ]
}

describe("SpeakersList", () => {

    it("snapshot default", () => {
        const result = shallowRenderComponent(<SpeakersListComponent speakers={speakers} />);

        expect(result).toMatchSnapshot();
    })

    it("snapshot loading", () => {
        const { asFragment } = renderComponent({ auth: {}, speakers: { loading: true } }, <SpeakersList />);

        expect(asFragment()).toMatchSnapshot();
    })

    it("snapshot error", () => {
        const { asFragment } = renderComponent({ auth: {}, speakers: { error: "errorMessage" } }, <SpeakersList />);

        expect(asFragment()).toMatchSnapshot();
    })

    it("get speakers when mount and updated", () => {
        const getSpeakers = jest.fn()
        const { rerender } = render(<SpeakersListComponent speakers={speakers} getSpeakers={getSpeakers} loggedIn={false} />);

        expect(getSpeakers).toBeCalledTimes(1); //mount

        rerender(<SpeakersListComponent speakers={speakers} getSpeakers={getSpeakers} loggedIn={false} />)

        expect(getSpeakers).toBeCalledTimes(1); //did update with false condition

        rerender(<SpeakersListComponent speakers={speakers} getSpeakers={getSpeakers} loggedIn={true} />)

        expect(getSpeakers).toBeCalledTimes(2); //mount + update
    })
})  