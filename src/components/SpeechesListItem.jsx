import React from "react";
import { Row, Col, Container } from 'react-bootstrap'
import { SpeakerListItem } from './SpeakerListItem'

const SpeechesListItem = ({ speech }) => {
    return (
        <Container style={{ padding: "5px" }}>
            <Row>
                <Col md={4}>
                    <h5>{speech.time}</h5>
                </Col>
                <Col md={8}>
                    <h5>{speech.title}</h5>
                    <SpeakerListItem speaker={speech.speaker} noImage />
                </Col>
            </Row>
        </Container>
    )
}

export default SpeechesListItem