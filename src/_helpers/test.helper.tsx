import React from 'react'
import { Provider } from 'react-redux'
import { StaticRouter } from 'react-router'
import { render } from '@testing-library/react'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import ShallowRenderer from 'react-test-renderer/shallow';
import { RootState } from '../redux/reducers'

export const renderComponent = (state: RootState, component: React.ReactElement) => {
    const mockStore = configureStore([thunk])
    const store = mockStore(state)
    const rendered = render(component,
        {
            wrapper: ({ children }) => <Provider store={store} >
                <StaticRouter>
                    {children}
                </StaticRouter>
            </Provider>
        }
    )
    return {
        ...rendered,
        store,
    }
}

export const shallowRenderComponent = (component: React.ReactElement) => {
    const renderer = ShallowRenderer.createRenderer();
    renderer.render(component);
    return renderer.getRenderOutput();
}