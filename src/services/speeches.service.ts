import { authHeader, handleResponse } from '../_helpers/service.helpers';
import { Speech } from '../types';

export const speechesService = {
    getAll,
};

function getAll(): Promise<Speech[]> {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`/speeches`, requestOptions).then(handleResponse)
}