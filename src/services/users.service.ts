import { authHeader, handleResponse } from '../_helpers/service.helpers';
import { User } from '../types/user';

export const usersService = {
    getAll,
};

function getAll(): Promise<User[]> {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`/users`, requestOptions).then(handleResponse)
}