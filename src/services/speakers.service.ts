import { authHeader, handleResponse } from '../_helpers/service.helpers';
import { Speaker } from '../types';

export const speakersService = {
    getAll,
};

function getAll(): Promise<Speaker[]> {
    const requestOptions: RequestInit = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`/speakers`, requestOptions).then(handleResponse)
}