import React from 'react'
import App from './App'
import { shallowRenderComponent } from './_helpers/test.helper'

describe("App", () => {

    it("snapshot default", () => {
        const result = shallowRenderComponent(<App />);

        expect(result).toMatchSnapshot();
    })
})  