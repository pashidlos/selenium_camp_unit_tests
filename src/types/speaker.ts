export interface Speaker {
    id: number,
    image: string,
    name: string,
    title: string,
    description: string
}