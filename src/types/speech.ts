export interface Speech {
    id: number,
    speakerId: number,
    time: string,
    title: string,
    description: string,
    score: number
}