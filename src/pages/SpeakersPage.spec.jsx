import React from 'react'
import SpeakersPage from './SpeakersPage'
import { shallowRenderComponent } from '../_helpers/test.helper'

describe("SpeakersPage", () => {

    it("snapshot default", () => {
        const result = shallowRenderComponent(<SpeakersPage />);

        expect(result).toMatchSnapshot();
    })
})  