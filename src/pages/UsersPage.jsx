import React from "react";
import UsersList from '../components/UserList'
import { Typography } from '@material-ui/core'

class UsersPage extends React.Component {
    render() {
        return (
            <div>
                <Typography variant='h3'>All registered users:</Typography>
                <UsersList />
            </div>
        )
    }
}

export default UsersPage