import React from "react";
import SpeakersList from '../components/SpeakerList'
import { Typography } from '@material-ui/core'

class SpeakersPage extends React.Component {
    render() {
        return (
            <div>
                <Typography variant='h3'>All speakers:</Typography>
                <SpeakersList />
            </div>
        )
    }
}

export default SpeakersPage