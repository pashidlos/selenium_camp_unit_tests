import React from 'react'
import UsersPage from './UsersPage'
import { shallowRenderComponent } from '../_helpers/test.helper'

describe("SpeechesPage", () => {

    it("snapshot default", () => {
        const result = shallowRenderComponent(<UsersPage />);

        expect(result).toMatchSnapshot();
    })
})  