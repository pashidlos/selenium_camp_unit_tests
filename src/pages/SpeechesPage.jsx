import React from "react";
import SpeechesList from '../components/SpeechesList'
import { Typography } from '@material-ui/core'

class SpeechesPage extends React.Component {
    render() {
        return (
            <div>
                <Typography variant='h3'>All speeches:</Typography>
                <SpeechesList />
            </div>
        )
    }
}

export default SpeechesPage