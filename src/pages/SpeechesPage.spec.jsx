import React from 'react'
import SpeechesPage from './SpeechesPage'
import { shallowRenderComponent } from '../_helpers/test.helper'

describe("SpeechesPage", () => {

    it("snapshot default", () => {
        const result = shallowRenderComponent(<SpeechesPage />);

        expect(result).toMatchSnapshot();
    })
})  