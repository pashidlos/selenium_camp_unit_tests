import { speakersConstants } from "../constants";
import { speakersService } from '../../services'

export const speakersActions = {
    getAll,
}

function getAll() {
    return dispatch => {
        dispatch(request())
        
        speakersService.getAll()
            .then(speakers => {
                dispatch(success(speakers));
            }).catch(error => {
                dispatch(failure(error.toString()));
            });;
    };

    function request() { return { type: speakersConstants.GETALL_REQUEST } }
    function success(speakers) { return { type: speakersConstants.GETALL_SUCCESS, speakers } }
    function failure(error) { return { type: speakersConstants.GETALL_FAILURE, error } }
};