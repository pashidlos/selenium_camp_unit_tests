import { speakersConstants } from "../constants";
import { speakersService } from '../../services'
import { Dispatch } from "redux";
import { Speaker } from "../../types";

export const speakersActions = {
    getAll,
}

function getAll() {
    return (dispatch: Dispatch) => {
        dispatch(request())
        
        speakersService.getAll()
            .then(speakers => {
                dispatch(success(speakers));
            }).catch(error => {
                dispatch(failure(error.toString()));
            });;
    };

    function request() { return { type: speakersConstants.GETALL_REQUEST } }
    function success(speakers: Speaker[]) { return { type: speakersConstants.GETALL_SUCCESS, speakers } }
    function failure(error: string) { return { type: speakersConstants.GETALL_FAILURE, error } }
};