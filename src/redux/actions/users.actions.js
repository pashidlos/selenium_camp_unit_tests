import { usersConstants } from "../constants";
import { usersService } from '../../services'

export const usersActions = {
    getAll,
}

function getAll() {
    return dispatch => {
        dispatch(request())
        
        usersService.getAll()
            .then(users => {
                dispatch(success(users));
            }).catch(error => {
                dispatch(failure(error.toString()));
            });;
    };

    function request() { return { type: usersConstants.GETALL_REQUEST } }
    function success(users) { return { type: usersConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: usersConstants.GETALL_FAILURE, error } }
};