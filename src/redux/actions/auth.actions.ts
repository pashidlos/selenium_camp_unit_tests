import { authConstants } from "../constants";
import { authService } from '../../services'
import { alertActions } from './alert.actions'
import { Dispatch } from "redux";
import { User } from "../../types/user";

export const authActions = {
    login,
    logout,
}

function login(user: User) {
    return (dispatch: Dispatch) => {
        dispatch(request(user));

        authService.login(user.email, user.password)
            .then(user => {
                dispatch(success(user));
            }).catch(error => {
                dispatch(alertActions.error(error.toString()));
            });
    };

    function request(user: User): IRequestAction { return { type: authConstants.LOGIN_REQUEST, payload: user } }
    function success(user: User): ISuccessAction { return { type: authConstants.LOGIN_SUCCESS, payload: user } }
};

function logout(): ILogoutAction {
    authService.logout();
    return { type: authConstants.LOG_OUT }
};

interface IRequestAction {
    type: typeof authConstants.LOGIN_REQUEST
    payload: User
}

interface ISuccessAction {
    type: typeof authConstants.LOGIN_SUCCESS
    payload: User
}

interface ILogoutAction {
    type: typeof authConstants.LOG_OUT
    payload?: undefined
}

export type AuthActionTypes = IRequestAction | ISuccessAction | ILogoutAction