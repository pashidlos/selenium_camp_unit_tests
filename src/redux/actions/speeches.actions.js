import { speechesConstants } from "../constants";
import { speechesService } from '../../services'

export const speechesActions = {
    getAll,
}

function getAll() {
    return dispatch => {
        dispatch(request())
        
        speechesService.getAll()
            .then(speeches => {
                dispatch(success(speeches));
            }).catch(error => {
                dispatch(failure(error.toString()));
            });;
    };

    function request() { return { type: speechesConstants.GETALL_REQUEST } }
    function success(speeches) { return { type: speechesConstants.GETALL_SUCCESS, speeches } }
    function failure(error) { return { type: speechesConstants.GETALL_FAILURE, error } }
};