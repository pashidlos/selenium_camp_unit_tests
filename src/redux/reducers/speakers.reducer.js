import { speakersConstants } from '../constants';

export function speakers(state = {}, action) {
  switch (action.type) {
    case speakersConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case speakersConstants.GETALL_SUCCESS:
      return {
        items: action.speakers
      };
    case speakersConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}