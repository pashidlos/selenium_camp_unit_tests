
import { combineReducers } from 'redux';
import { auth } from './auth.reducer';
import { users } from './users.reducer';
import { speakers } from './speakers.reducer';
import { speeches } from './speeches.reducer';
import { alert } from './alert.reducer';

const rootReducer = combineReducers({
  auth,
  users,
  speakers,
  speeches,
  alert
});

export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>