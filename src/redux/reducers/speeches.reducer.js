import { speechesConstants } from '../constants';

export function speeches(state = {}, action) {
  switch (action.type) {
    case speechesConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case speechesConstants.GETALL_SUCCESS:
      return {
        items: action.speeches
      };
    case speechesConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}