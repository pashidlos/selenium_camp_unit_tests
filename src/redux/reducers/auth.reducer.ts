import { authConstants } from '../constants';
import { AuthActionTypes } from '../actions/auth.actions'
import { User } from '../../types/user';

export interface IAuthReducer {
  loggedIn: boolean
  user?: User
}

const localStorageUser = localStorage.getItem('user')

const initialState: IAuthReducer = {
  loggedIn: false,
  user: localStorageUser ? JSON.parse(localStorageUser) : undefined,
};

export function auth(state = initialState, action: AuthActionTypes): IAuthReducer {
  switch (action.type) {
    case authConstants.LOGIN_REQUEST:
      return {
        loggedIn: false,
        user: action.payload
      };
    case authConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.payload
      };
    case authConstants.LOG_OUT:
      return initialState;
    default:
      return state
  }
}