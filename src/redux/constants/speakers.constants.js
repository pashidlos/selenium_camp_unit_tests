export const speakersConstants = {
    GETALL_REQUEST: 'speakers@GETALL_REQUEST',
    GETALL_SUCCESS: 'speakers@GETALL_SUCCESS',
    GETALL_FAILURE: 'speakers@GETALL_FAILURE',
};